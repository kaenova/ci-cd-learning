const express = require('express')
const app = express()
const port = process.env.PORT || 3000

app.get('/', (req, res) => {
  res.send('Hello World!')
})

app.get('/cicd', (req, res) => {
  res.send('This is CICD')
})

app.get('/cicd', (req, res) => {
  res.send('This is final commit, the machine wont be used anymore')
})


app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`)
})